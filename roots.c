#include <stdio.h>
#include <math.h>
int main()
{
    int a,b,c;
    float D, deno, root1,root2;
    printf("Enter the values of a,b,and c:\n");
    scanf("%d%d%d",&a,&b,&c);
    D = (b *b) - (4 *a *c);
    deno = 2*a;
    if(D>0)
    {
        printf("REAL ROOTS\n");
        root1 = (-b + sqrt(D))/deno;
        root2 = (-b - sqrt(D))/deno;
        printf("root1=%f\n root2=%f",root1,root2);
    }
    else if(D==0)
    {
        printf("ROOTS ARE EQUAL\n");
        root1=(-b)/deno;
        printf("root1=%f\n root2=%f",root1,root2);
    }
    else
    printf("IMAGINARY ROOTS\n");
    return 0;
}