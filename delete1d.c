#include<stdio.h>
int main()
{
    int n,pos,num;
    printf("Enter the total number of elements\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the elements\n");
    
    for(int i = 0;i < n; i++)
    {
        scanf("%d",&a[i]);
    }
    
    printf("Enter the number to be deleted\n");
    scanf("%d",&num);
    
    printf("Enter the position at which %d is located\n",num);
    scanf("%d",&pos);
    
    for(int j = pos;j < n; j++)
    {
        a[j] = a[j + 1];
    }
    
    n = n - 1;
    
    printf("The values are \n");
    
    for(int k = 0; k < n;k++)
    {
        printf("%d\n",a[k]);
    }
    
    return 0;
}