#include <stdio.h>

int main()
{
    int a[10],num,i,n,beg,end,mid,found=0;
    printf("\nEnter the number of elements in an array:");
    scanf("%d",&n);
    
    printf("\nEnter the number:");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("\nEnter the number that has to be searched:");
    scanf("%d",&num);
    
    beg=0;end=n-1;
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(a[mid]==num)
        {
        printf("\n %d is present in the array at position=%d",num,mid+1);
        found=1;
        break;
        }
        if(a[mid]>num)
        end=mid-1;
        else 
        beg=mid+1;
    }
    if(beg>end && found==0)
    printf("\n%d does not exist in the array",num);
    return 0;
}